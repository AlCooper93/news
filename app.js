const selectCountry = document.querySelector('.country');
const selectCategory = document.querySelector('.category')
const input = document.querySelector('input');
const selectBTN = document.querySelectorAll('select')

const service = new NewService();
const ui = new NewsUI();

selectBTN.forEach((select)=> select.addEventListener('change', getNews))
input.addEventListener('input', getNews);

function getNews() {
    if(selectCountry.value&&selectCategory.value){
        let country = selectCountry.value;
        let category = selectCategory.value;
        let search = input.value;
        
        service.getData(country, category, search)
            .then((res) => {
                ui.renderAllNews(res.articles)
                console.log(res)
            });
    }
}