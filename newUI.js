class NewsUI {
    constructor() {
        this.articles = [];
    }

    renderAllNews(arrayNews) {
        this.articles = arrayNews;
        let main = document.querySelector(".main-content");
        main.innerHTML = "";
        this.articles.forEach((news) => {
            let node = this.renderOneNews(news);
            main.insertAdjacentHTML("afterbegin", node)
        });
    }

    renderOneNews(news) {
        return `<div class="one-news">
                    <a class="link-news" href="${news.url}">
                    <h3>${news.title}</h3>
                    <img class="img-news" src="${news.urlToImage}">
                    ${news.description}</a>
        </div>`;
        console.log(news.title, news.content, news.source.name)
    }
}